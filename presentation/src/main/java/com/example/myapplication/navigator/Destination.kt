package com.example.myapplication.navigator

sealed class Destination {
    sealed class Watchlist : Destination() {
        data class Trade(val nscCode: String) : Watchlist()
        object Test : Destination()
    }

    sealed class Portfolio : Destination() {
    }

    sealed class More : Destination() {
        object Accounts : More()
    }

    sealed class Global : Destination() {
        data class Search(val onlyInstruments: Boolean = true) : Global()
        data class InstrumentDetail(val nscCode: String) : Global()

        sealed class BottomSheet : Global() {
            data class Warning(val message: String) : BottomSheet()
        }
    }

    sealed class DeepLink {

    }

}
