package com.example.myapplication.navigator

sealed class NavEvent

data class To(val destination: Destination) : NavEvent()
object Up : NavEvent()
object Back : NavEvent()
object BackToRoot : NavEvent()

infix fun Navigator.navigateTo(destination: Destination) {
    navigate(To(destination))
}
