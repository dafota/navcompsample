package com.example.myapplication.navigator

interface Navigator {
    fun navigate(event: NavEvent)
}