package com.example.myapplication

import androidx.lifecycle.ViewModel
import com.example.myapplication.navigator.Destination
import com.example.myapplication.navigator.NavEvent
import com.example.myapplication.navigator.Navigator
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class RootViewModel @Inject constructor(val navigator: Navigator) : ViewModel() {

    fun navigate(event: NavEvent) {
        navigator.navigate(event)
    }

    private fun navigateToInstrumentDetail() {
        navigator to Destination.Global.InstrumentDetail("nscCode")
    }
}