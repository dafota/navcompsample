package com.example.myapplication

import androidx.lifecycle.ViewModel
import com.example.myapplication.navigator.Destination
import com.example.myapplication.navigator.Navigator
import com.example.myapplication.navigator.navigateTo
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class MoreViewModel @Inject constructor(
    private val navigator: Navigator
) : ViewModel() {

    fun navigateToAccounts() {
        navigator navigateTo  Destination.More.Accounts
    }

}