package com.example.navadvanced.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.navadvanced.databinding.DialogSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class WarningBottomSheet : BaseBottomSheetDialog<DialogSimpleTextBinding>() {

    override fun inflateBinding(inflater: LayoutInflater, container: ViewGroup?) =
        DialogSimpleTextBinding.inflate(inflater, container, false)

    private val args by lazy { WarningBottomSheetArgs.fromBundle(requireArguments()) }

    override fun setUp() {
        binding.tvText.text = args.message
    }
}