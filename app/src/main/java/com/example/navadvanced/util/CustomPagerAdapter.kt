package com.example.navadvanced.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter

class CustomPagerAdapter(activity: FragmentActivity, count: Int, factory: (Int) -> Fragment) :
    FragmentStateAdapter(activity) {

    private var count: Int = -1
    private lateinit var factory: (Int) -> Fragment

    init {
        init(count, factory)
    }

    private fun init(count: Int, factory: (Int) -> Fragment) {
        this.count = count
        this.factory = factory
    }


    override fun getItemCount() = count

    override fun createFragment(position: Int) = factory(position)
}