package com.example.navadvanced.util

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

fun ViewPager2.setupWithTabLayout(
    tabLayout: TabLayout,
    tabConfigurationStrategy: (TabLayout.Tab, Int) -> Unit
) {
    TabLayoutMediator(tabLayout, this, true, true, tabConfigurationStrategy).attach()
}


fun ViewPager2.currentFragment(
    fragmentManager: FragmentManager
): Fragment? {
    return fragmentManager.findFragmentByTag("f$currentItem")
}

fun <T> LifecycleOwner.observe(liveData: LiveData<T>, observer: Observer<T>) {
    liveData.observe(this, observer)
}