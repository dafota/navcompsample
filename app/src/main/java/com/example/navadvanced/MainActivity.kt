package com.example.navadvanced

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.myapplication.RootViewModel
import com.example.myapplication.navigator.Destination
import com.example.myapplication.navigator.To
import com.example.navadvanced.databinding.ActivityMainBinding
import com.example.navadvanced.navigator.DefaultNavigator
import com.example.navadvanced.navigator.NavHost
import com.example.navadvanced.util.observe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber

private const val TAG = "MainActivity"

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), NavHost {

    override lateinit var navController: NavController
    private lateinit var appBarConfiguration: AppBarConfiguration

    private val viewModel by viewModels<RootViewModel>()
    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.nav_host_fragment
        ) as NavHostFragment

        navController = navHostFragment.navController

        // Setup the bottom navigation view with navController
        binding.bottomNav.setupWithNavController(navController)

        binding.bottomNav.setOnItemReselectedListener {
            when (it.itemId) {
                R.id.nav_watchlist -> navController.popBackStack(
                    destinationId = R.id.watchlistFragment,
                    inclusive = false,
                )
                R.id.nav_portfolio -> navController.popBackStack(
                    destinationId = R.id.portfolioFragment,
                    inclusive = false,
                )
                R.id.nav_order -> navController.popBackStack(
                    destinationId = R.id.orderFragment,
                    inclusive = false,
                )
                R.id.nav_social -> navController.popBackStack(
                    destinationId = R.id.socialFragment,
                    inclusive = false,
                )
                R.id.nav_more -> navController.popBackStack(
                    destinationId = R.id.moreFragment,
                    inclusive = false,
                )
            }
        }


        // Setup the ActionBar with navController and 5 top level destinations
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.watchlistFragment,
                R.id.portfolioFragment,
                R.id.socialFragment,
                R.id.orderFragment,
                R.id.moreFragment
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)

        observe((viewModel.navigator as DefaultNavigator).navEvents) {
            it(navController)
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_search -> viewModel.navigate(To(Destination.Global.Search(onlyInstruments = false)))
            R.id.menu_test -> viewModel.navigate(To(Destination.Watchlist.Test))
            R.id.menu_warning -> viewModel.navigate(To(Destination.Global.BottomSheet.Warning("Just for test.")))
            android.R.id.home -> navController.popBackStack()
        }
        return true
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp(appBarConfiguration)
    }

}