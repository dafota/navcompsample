package com.example.navadvanced.di

import com.example.myapplication.navigator.Navigator
import com.example.navadvanced.navigator.DefaultNavigator
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent


@Module
@InstallIn(ActivityRetainedComponent::class)
interface PresentationBindings {

    @Binds
    fun bindNavigator(defaultNavigator: DefaultNavigator): Navigator

}