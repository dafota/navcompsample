package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.navadvanced.databinding.FragmentSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PagerItemFragment : BaseFragment<FragmentSimpleTextBinding>() {

    companion object {
        const val EXTRA_INDEX = "extra_index"

        fun newInstance(index: Int): PagerItemFragment {
            val args = Bundle()
            args.putInt(EXTRA_INDEX, index)
            val fragment = PagerItemFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSimpleTextBinding.inflate(inflater, container, false)

    override fun setUp() {
        binding.tvText.text = requireArguments().getInt(EXTRA_INDEX).toString()
        binding.tvText.text = this::class.java.canonicalName
    }
}