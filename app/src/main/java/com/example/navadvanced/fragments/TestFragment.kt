package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.navadvanced.databinding.FragmentSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
class TestFragment : BaseFragment<FragmentSimpleTextBinding>() {

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): FragmentSimpleTextBinding {
        return FragmentSimpleTextBinding.inflate(inflater, container, false)
    }

    override fun setUp() {
        binding.tvText.text = UUID.randomUUID().toString()
    }
}