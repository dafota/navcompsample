package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.example.myapplication.MoreViewModel
import com.example.navadvanced.databinding.FragmentMoreBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoreFragment : BaseFragment<FragmentMoreBinding>() {

    private val moreViewModel by viewModels<MoreViewModel>()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentMoreBinding.inflate(inflater, container, false)

    override fun setUp() {
        binding.btnToAccounts.setOnClickListener {
            moreViewModel.navigateToAccounts()
        }

        Toast.makeText(requireContext(), "More", Toast.LENGTH_SHORT).show()
    }
}