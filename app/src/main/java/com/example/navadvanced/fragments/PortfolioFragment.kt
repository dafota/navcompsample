package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.navGraphViewModels
import com.example.myapplication.RootViewModel
import com.example.navadvanced.R
import com.example.navadvanced.databinding.FragmentSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PortfolioFragment :BaseFragment<FragmentSimpleTextBinding>() {

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSimpleTextBinding.inflate(inflater, container, false)

    override fun setUp() {
        binding.tvText.text = this::class.java.canonicalName

    }
}