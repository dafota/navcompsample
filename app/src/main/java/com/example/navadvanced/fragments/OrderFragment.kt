package com.example.navadvanced.fragments

import androidx.navigation.navGraphViewModels
import com.example.myapplication.RootViewModel
import com.example.navadvanced.R
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

private const val TAG = "OrderFragment"


@AndroidEntryPoint
class OrderFragment: PagerFragment() {

    private val childViewModel by navGraphViewModels<RootViewModel>(R.id.nav_watchlist)

}