package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import com.example.myapplication.WatchlistViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class WatchlistFragment : PagerFragment() {

    private val watchlistViewModel by viewModels<WatchlistViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnPagerNavigate.setOnClickListener {
            watchlistViewModel.navigateToInstrumentDetail()
        }
    }

}
