package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.navadvanced.databinding.FragmentSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class InstrumentDetailFragment : BaseFragment<FragmentSimpleTextBinding>() {

    private val args by lazy { InstrumentDetailFragmentArgs.fromBundle(requireArguments()) }

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSimpleTextBinding.inflate(inflater, container, false)

    override fun setUp() {
        binding.tvText.text = "nscCode: ${args.nscCode}"
    }
}