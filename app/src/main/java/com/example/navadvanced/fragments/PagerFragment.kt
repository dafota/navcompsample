package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.example.navadvanced.databinding.FragmentPagerBinding
import com.example.navadvanced.util.CustomPagerAdapter
import com.example.navadvanced.util.setupWithTabLayout
import dagger.hilt.android.AndroidEntryPoint
import java.util.*


@AndroidEntryPoint
abstract class PagerFragment : Fragment() {

    private var _binding: FragmentPagerBinding? = null
    val binding by lazy { _binding!! }

    @CallSuper
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPagerBinding.inflate(inflater, container, false)
        return binding.root
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding.viewPager) {
            adapter = CustomPagerAdapter(requireActivity(), 3) {
                PagerItemFragment.newInstance(it)
            }
            //offscreenPageLimit = 1

            setupWithTabLayout(binding.tabLayout) { tab, i ->
                tab.text = "#${i}"
            }

            isSaveEnabled = false
        }

        binding.tvUuid.text = UUID.randomUUID().toString()
    }


    @CallSuper
    override fun onDestroyView() {
        super.onDestroyView()
        binding.viewPager.adapter = null
        _binding = null
    }
}