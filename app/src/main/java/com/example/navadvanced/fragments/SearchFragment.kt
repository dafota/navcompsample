package com.example.navadvanced.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.example.myapplication.RootViewModel
import com.example.navadvanced.databinding.FragmentSimpleTextBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSimpleTextBinding>() {

    companion object {
        const val EXTRA_ONLY_INSTRUMENTS = "only_instruments"
    }

    private val args by lazy { SearchFragmentArgs.fromBundle(requireArguments()) }
    private val viewModel by viewModels<RootViewModel>()

    override fun inflateBinding(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentSimpleTextBinding.inflate(inflater, container, false)

    override fun setUp() {
        binding.tvText.text = "only instruments ${args.onlyInstruments}"
    }

}