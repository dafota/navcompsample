package com.example.navadvanced.navigator

import androidx.navigation.NavController

interface NavHost {
    val navController: NavController
}