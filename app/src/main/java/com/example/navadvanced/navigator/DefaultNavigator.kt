package com.example.navadvanced.navigator

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavController
import com.example.myapplication.navigator.*
import com.example.navadvanced.NavRootDirections
import com.example.navadvanced.fragments.MoreFragmentDirections
import com.example.navadvanced.fragments.WatchlistFragmentDirections
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

typealias NavCommand = NavController.() -> Unit

@ActivityRetainedScoped
class DefaultNavigator @Inject constructor() : Navigator {

    private val _navEvents = MutableLiveData<NavCommand>()
    val navEvents: LiveData<NavCommand> = _navEvents

    @MainThread
    override fun navigate(event: NavEvent) {
        _navEvents.value = {
            when (event) {
                Back -> popBackStack()
                BackToRoot -> TODO()
                Up -> navigateUp()
                is To -> navigate(event.destination)
            }
        }
    }
}

private fun NavController.navigate(event: Destination) {
    when (event) {
        is Destination.Global.Search -> navigate(NavRootDirections.toSearchFragment(event.onlyInstruments))
        is Destination.Global.InstrumentDetail ->
            navigate(NavRootDirections.toInstrumentDetail(event.nscCode))
        is Destination.Watchlist.Trade -> TODO()
        Destination.Watchlist.Test -> navigate(WatchlistFragmentDirections.actionWatchlistFragmentToTestFragment())
        Destination.More.Accounts -> navigate(MoreFragmentDirections.actionMoreFragmentToAccountsFragment())
        is Destination.Global.BottomSheet.Warning ->
            navigate(NavRootDirections.toWarningDialog(event.message))

        else -> {
            throw IllegalStateException("Direction $event is not implemented yet.")
        }
    }
}
